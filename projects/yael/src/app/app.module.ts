import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';

import { HttpMockRequestInterceptorProvider } from './shared/http-interceptors/http-mock-request.interceptor';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [
        HttpMockRequestInterceptorProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
