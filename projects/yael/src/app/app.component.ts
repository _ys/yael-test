import { Component, ChangeDetectionStrategy } from '@angular/core';
import { MockRequestApiService } from './shared/services/mock-request-api.service';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface MockData {
    [key: string]: any
}

// HELPER FUCTIONS
export function getUniqueFields(data) {
    let set = new Set(data.map(v => Object.keys(v)).flat());
    return Array.from(set.keys());
}

export function getNormalizedMockSingle(uniqueFieldsArray) {
    const uniqueFieldsObject = {};
    uniqueFieldsArray.forEach((v: string) => uniqueFieldsObject[v] = null);
    return uniqueFieldsObject;
}

export function getNormalizedMockData(data, uniqueFieldsObject) {
    return data.map(v => Object.assign({}, uniqueFieldsObject, v)).slice();
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.pug',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
    title = 'test';
    data: any;
    key: string;

    tableHeaders = new BehaviorSubject<any[]>(null);
    tableData = new BehaviorSubject<MockData[]>(null);
    normalizedMockDataSingle;
    editableCellNewValue;

    constructor(
        private mockRequestApiService: MockRequestApiService
    ) {}

    ngOnInit() {
        this.mockRequestApiService.getMockData()
            .pipe(
                map((data: MockData) => data.result.slice())
            )
            .subscribe((data: MockData[]) => {
                // console.log(data);
                const uniqueFieldsArray = getUniqueFields(data);
                this.tableHeaders.next(uniqueFieldsArray);
                this.normalizedMockDataSingle = getNormalizedMockSingle(uniqueFieldsArray);
                this.tableData.next(getNormalizedMockData(data, this.normalizedMockDataSingle));
                // this.tableData.next(data);
            });
    }

    addDataRow() {
        this.mockRequestApiService.addMockData()
            .subscribe((res: any) => {
                console.log(res);
                this.tableData.value.push(Object.assign({}, this.normalizedMockDataSingle, { animalId: res.id }));
                this.tableData.next(this.tableData.value);
            });
    }

    updateDataCell(id, data) {
        this.mockRequestApiService.updateMockData(id, data)
            .subscribe(res => console.log(res));
    }

    deleteDataRow(id, index) {
        this.mockRequestApiService.deleteMockData(id)
            .subscribe(res => {
                console.log(res);
                this.tableData.value.splice(index, 1);
                this.tableData.next(this.tableData.value);
            });
    }

    isEditable(key: string) {
        return key.toString().indexOf('Id') < 0;
    }

    onContentChange(innerHTML: string) {
        // console.log(innerHTML);
        this.editableCellNewValue = innerHTML;
    }

    saveChange(id: string, key: string) {
        this.updateDataCell(id, { [key]: this.editableCellNewValue });
    }
}
