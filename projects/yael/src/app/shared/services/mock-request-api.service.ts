import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MockRequestApiService {

    constructor(
        private http: HttpClient
    ) {}

    getMockData () {
        return this.http.get('https://data/manipulation');
    }

    addMockData() {
        return this.http.post('https://data/manipulation', {});
    }

    updateMockData(id: string, data: object) {
        return this.http.put('https://data/manipulation', { id, data });
    }

    deleteMockData(id: string) {
        return this.http.delete('https://data/manipulation', { params: { id }});
    }

}
