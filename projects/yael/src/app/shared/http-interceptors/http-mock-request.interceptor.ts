import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import * as data from '../../mock-data/table-data.json';

const MOCK_URL = 'https://data/manipulation';

@Injectable()
export class HttpMockRequestInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log(request);

        if (request.url === MOCK_URL) {
            console.log(`intercepted \n url ${request.url} \n method ${request.method}`);
            let body;
            switch (request.method) {
                case 'GET':
                    body = (data as any).default;
                    break;
                case 'POST':
                    body = { id: +Math.random().toString().slice(-4) }
                    break;
                case 'PUT':
                    body = { id: request.body.id, data: request.body.data }
                    break;
                case 'DELETE':
                    body = { id: request.params.get('id') }
                    break;
                default:
                    break;
            }
            return of(new HttpResponse({ status: 200, body }));
        }

        return next.handle(request);
    }
}

export const HttpMockRequestInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpMockRequestInterceptor,
    multi: true
};
